/*
 * Template class that will be used to hold pointers to things
 * Sorted array list, depends on operator< existing for the type being held
*/

#ifndef ARRAY_LIST_HPP
#define ARRAY_LIST_HPP

// Any necessary includes
#include<iostream>
#include<memory>
#include<stdexcept>
#include<list>


// It's a template class
template<class T>
class List {
public:
    // Default Constructor
    List();
    // Constructor that takes first item of list
    List(T first_elem);
    // Copy Constructor
    List(const List<T>& ListObject);
    // Move Constructor
    List(List<T>&& ListObject);
    // Destructor
    ~List();
    // Copy assignment operator overload
    List& operator = (const List<T>& ListObj);
    // Move assignment operator overload
    List& operator = (List<T>&& ListObj);
    // function size; does not throw exceptions
    size_t size()const noexcept {return(_size);};
    // function capacity; does not throw exceptions
    size_t capacity()const noexcept {return(_capacity);};
    // function insert; does not throw exceptions
    void insert(const T& element);
    // function erase; throws underflow_error if empty, range_error if item doesn't exist
    void erase(T item);
    // function at; throws out_of_range exception; returns a const value
    const T& at(int index)const;
    // function at; throws out_of_range exception; returns l-value
    T& at(int index);
    // function search; throws domain error if item doesn't exist
    size_t search(T obj);

    void sort_list();
private:
    // variable that gives us our dynamic array
    T* dynamic_array;
    size_t _size;
    size_t _capacity;
    void increase_array();

};


// LIST CLASS FUNCTION IMPLEMENTATIONS
template<class T>
List<T> :: List()
    :dynamic_array(nullptr)
    ,_size(0)
    ,_capacity(0)

{
};

template<class T>
List<T> :: List(T first_elem)
:_size(1),_capacity(1)
{
    dynamic_array = new T[1];
    dynamic_array[0]= first_elem;
};

// List<T>::List(const List& ListObject){
template<class T>
List<T>::List(const List<T>& ListObject)
    :_size(ListObject._size),_capacity(ListObject._size)
    //,dynamic_array(ListObject.dynamic_array)
{
    dynamic_array = new T[_capacity];
	for (size_t i = 0; i<_size; i++){
	   dynamic_array[i] = ListObject.dynamic_array[i];//Copies the contents of the Object.
    }

};
template<class T>
List<T>::List(List<T>&& ListObject)
    :_size(ListObject._size),_capacity(ListObject._capacity)
    //,dynamic_array(std::move(ListObject.dynamic_array))
{
    dynamic_array = new T[_capacity];
	dynamic_array = ListObject.dynamic_array;//tranfers the ownership.
	ListObject.dynamic_array = nullptr;//makes the object point to null.
};

template<class T>
List<T>::~List(){
    delete [] dynamic_array;
    dynamic_array = nullptr;
}
template<typename T>
List<T>& List<T>::operator= (const List<T>& ListObj)
// :_size(ListObject._size),_capacity(ListObject._capacity)
{
    delete[] dynamic_array;
    dynamic_array = new T[ListObj._capacity];
    _capacity = ListObj._capacity;
    _size = ListObj._size;

    for(size_t i = 0; i<_size; i++)
        dynamic_array[i] = ListObj.dynamic_array[i];

    return *this;
}
template<typename T>
List<T>& List<T>::operator= (List<T>&& ListObj)
{
    delete[] dynamic_array;
    dynamic_array = new T[ListObj._capacity];
    _capacity = ListObj._capacity;
    _size = ListObj._size;
    ListObj.dynamic_array = nullptr;

    return *this;

}

template<class T>
void List<T>::increase_array(){
    if(_size == _capacity){
        T* temp_array = new T[_capacity*2];


        for (size_t i = 0; i < _size; ++i)
        {
            temp_array[i] = std::move(dynamic_array[i]);
        }
        delete [] dynamic_array;
        temp_array = dynamic_array;
        _capacity *=2;
    }

}

template<class T>
void List<T>::insert(const T& element)
{
    if (_size+1 > _capacity){
	    if(_capacity == 0)
            _capacity = 1;
        // for(int cap = _capacity; cap<= _size; cap = cap* 2 ){
        //     auto Listptr = std::make_shared<List[]>(_capacity);
        //     for(unsigned int i = 0; i < _size; i++)
        //         Listptr[i] = dynamic_array[i];
        // }
    }
	dynamic_array[_size] = element;
	_size++;
}
template<class T>
size_t List<T>::search(T obj){

    for(size_t i =0;i<_size;++i){
        if(dynamic_array[i] == obj){
            return(i);
        }else{
            throw(std::domain_error("domain_error"));
        }
    }

}
template<class T>
void List<T>::erase(T item)
{
    if(_size == 0){
        throw  std::underflow_error("Empty List!");
    }
    // auto Listptr = std::make_shared<List[]>(_capacity);
    // std::list<T> temp (third);
    // for (std::list<T>::iterator it=dynamic_array.begin(); it!=dynamic_array.end(); ++it)
        // Listptr[i] = dynamic_array[i];
        size_t index  = this->search(item);
        if(index){
            for(size_t i =index ; i<=_size; i++){
                dynamic_array[i] = dynamic_array[i+1];
            // if(dynamic_array[i] == item){
            //     delete dynamic_array[i];
                // for(unsigned int j = i ; j<_size;j++){
                //     dynamic_array[j] = dynamic_array[j+1];
                // }
                // break;
                }
            }else{
                throw(std::range_error("range_error"));
            }
            _size--;
}
template<class T>
const T& List<T>::at(int index)const{
    if(index < 0 || index >= _size){
        throw std::out_of_range("out_of_range");
    }else{
        return(dynamic_array[index]);
    }
}
template<class T>
T& List<T>::at(int index){
    if(index < 0 || index >= _size){
        throw std::out_of_range("out_of_range");
    }else{
        return(dynamic_array[index]);
    }
}
template<class T>
void List<T>::sort_list(){
    {
   int i, j;
   bool swapped;
   for (size_t i = 0; i < _size-1; i++)
   {
     swapped = false;
     for (size_t j = 0; j < _size-i-1; j++)
     {
        if (dynamic_array[j] > dynamic_array[j+1])
        {
           // swap(&dynamic_array[j], &dynamic_array[j+1]);
           T temp = dynamic_array[j];
           dynamic_array[j] = dynamic_array[j+1];
           dynamic_array[j+1] = temp;

           swapped = true;
        }
     }

     // IF no two elements were swapped by inner loop, then break
     if (swapped == false)
        break;
   }
}

}




#endif
