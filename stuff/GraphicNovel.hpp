#ifndef GRAPHIC_NOVEL_HPP
#define GRAPHIC_NOVEL_HPP

#include<iostream>
#include<string>
#include"Book.hpp"


class GraphicNovel:public Book {
    public:
        GraphicNovel(std::string last,std::string first,std::string illus,std::string title,int year);
        std::ofstream& print(std::ofstream& out)const override{
            out<<this->author();
            return(out);
        };
        // std::ostream& operator << (const Book& Obj)const;
        std::string illustrator(){return(illus);};
    protected:
	    std::string illus;
};

#endif
