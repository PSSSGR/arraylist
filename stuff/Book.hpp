#ifndef BOOK_HPP
#define BOOK_HPP

#include<iostream>
#include<string>
#include <fstream>

std::ofstream out;

class Book {
//An abstract class.
    public:
        Book(std::string first,std::string last,std::string title,int year);
        // virtual ~Book();
        virtual std::ofstream& print(std::ofstream& out)const = 0;
        std::ostream& operator << (const Book& Obj)const{
            Obj.print(out);
            return(out);
        }

        bool operator > (Book& Obj)const{
            std::string temp1 = First_name +" "+ Last_name;
            std::string temp2 = Obj.author();
            std::string temp3 = Title_name;
            std::string temp4 = Obj.title();

            if(temp1 > temp2 ){
                return(true);
            }else if(temp1 == temp2){
                if(temp3 > temp4){
                    return(true);
                }else{
                    return(false);
                }
            }else{
                return(false);
            }
        }
        std::string author()const{return(First_name + " " + Last_name);}
        std::string title()const{return(Title_name);}
        int year()const{return(published_year);}
        void set_first(std::string first){First_name=first;}
        void set_title(std::string title){Title_name = title;}
        void set_last(std::string last){Last_name = last;}
        void set_year(int time){published_year = time;}


    protected:

	    std::string Last_name;
        std::string First_name;
        std::string Title_name;
	    int published_year;
};

#endif
