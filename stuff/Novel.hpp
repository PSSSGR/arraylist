#ifndef NOVEL_HPP
#define NOVEL_HPP
#include "Book.hpp"
#include <iostream>
#include <string>


class Novel : public Book {
    public:
    Novel(std::string first,std::string last,std::string title,int year)
    :Book(first,last,title,year){};
        // {
        //     set_first(first);
        //     set_last(last);
        //     set_title(title);
        //     set_year(year);
        // }

    std::ofstream& print(std::ofstream& out) const override {
        out<<this->author();
        return(out);
    };
};

#endif
