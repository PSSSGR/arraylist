/*
 * Filename:	GraphicNovel.cpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#include <iomanip>
#include "GraphicNovel.hpp"

GraphicNovel::GraphicNovel(
    std::string last,
    std::string first,
    std::string illustrator,
    std::string title,
    int year
) :
    Book(last, first, title, year),
    _illustrator(illustrator)
{
}

std::ostream& GraphicNovel::print(std::ostream &sout) const
{
    const int F_WIDTH = 14;
    char fill = std::cout.fill('.');

    sout << std::left
         << std::setw(F_WIDTH) << "Author" << _forename << " " << _surname << std::endl
         << std::setw(F_WIDTH) << "Illustrator" << _illustrator << std::endl
         << std::setw(F_WIDTH) << "Title" << _title << std::endl
         << std::setw(F_WIDTH) << "Year" << _year << std::endl;

    std::cout.fill(fill);

    return sout;
}
