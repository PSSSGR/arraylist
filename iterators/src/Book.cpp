/*
 * Filename:	Book.cpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */

#include "Book.hpp"

Book::Book(
    std::string last,
    std::string first,
    std::string title,
    int year
) :
    _surname(last),
    _forename(first),
    _title(title),
    _year(year)
{
}

std::string Book::author() const
{
    return _forename + " " + _surname;
}

std::string Book::title() const
{
    return _title;
}

int Book::year() const
{
    return _year;
}

bool operator==(const Book &lhs, const Book &rhs)
{
    return (!(lhs < rhs) && !(rhs < lhs));
}

bool operator!=(const Book &lhs, const Book &rhs)
{
    return !(lhs == rhs);
}

bool operator< (const Book &lhs, const Book &rhs)
{
    if (lhs._surname < rhs._surname) {
        return true;
    } else if (lhs._surname == rhs._surname) {
        if (lhs._forename < rhs._forename) {
            return true;
        } else if (lhs._title < rhs._title) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

bool operator<=(const Book &lhs, const Book &rhs)
{
    return !(rhs < lhs);
}

bool operator> (const Book &lhs, const Book &rhs)
{
    return rhs < lhs;
}

bool operator>=(const Book &lhs, const Book &rhs)
{
    return !(lhs < rhs);
}

std::ostream& operator<<(std::ostream &sout, const Book &rhs)
{
    return rhs.print(sout);
}
