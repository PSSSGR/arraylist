/*
 * Template class that will be used to hold pointers to things
 * Sorted array list, depends on operator< existing for the type being held
*/
/*
 * Filename:	List.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#ifndef ARRAY_LIST_HPP
#define ARRAY_LIST_HPP

// Any necessary includes
#include<iostream>
#include<memory>// std::allocator class
#include<stdexcept>
#include <string>
#include <utility>


// It's a template class
template<class T>
class List {
public:
    // Default Constructor
    List();
    // Constructor that takes first item of list
    List(T first_elem);
    // Copy Constructor
    List(const List<T>& ListObject);
    // Move Constructor
    List(List<T>&& ListObject);
    // Destructor
    ~List();
    // Copy assignment operator overload
    List<T>& operator=(const List<T> &rhs);
   // Move assignement operator overload
    List<T>& operator=(List<T> &&rhs);
    // function size; does not throw exceptions
    size_t size()const noexcept{return _end - _begin;};
    // function capacity; does not throw exceptions
    size_t capacity()const noexcept{return _capacity - _begin;};
    // function insert; does not throw exceptions
    void insert(const T& element)noexcept;
    // function erase; throws underflow_error if empty, range_error if item doesn't exist
    void erase(T item);
    // function at; throws out_of_range exception; returns a const value
    T& at(size_t index);
    // function at; throws out_of_range exception; returns l-value
    const T& at(size_t index) const;
    // function search; throws domain error if item doesn't exist
    size_t search(const T& obj)const;
private:
    // variable that gives us our dynamic array
   static std::allocator<T> alloc;
   T * _begin;
   T * _end;
   T * _capacity;
   void grow() noexcept;//extra
   void displace(unsigned int i);//extra
   void collapse(unsigned int i) noexcept;//extra
};
/*** Static Initialization ***/
template<class T>
std::allocator<T> List<T>::alloc;

// LIST CLASS FUNCTION IMPLEMENTATIONS
template<class T>
List<T> :: List()
    :_begin(nullptr)
    ,_end(nullptr)
    ,_capacity(nullptr)
{
};
template <typename T>
List<T>::List(T item)
    : _begin(alloc.allocate(1))
    ,_end(_begin)
    ,_capacity(_begin + 1)
{
    alloc.construct(_end++, item);
}

template <typename T>
List<T>::List(const List<T> &rhs)
    : _begin(alloc.allocate(rhs.size()))
    , _end(_begin)
    , _capacity(_begin + rhs.size())
{
    for(size_t i = 0; i < rhs.size(); i++){
        alloc.construct(_end++, rhs._begin[i]);//constructing the new list.
    }
}
template <typename T>
List<T>::List(List<T> &&rhs)
    :_begin(rhs._begin)
    ,_end(rhs._end)
    ,_capacity(rhs._capacity)
{
    rhs._begin = rhs._end =rhs._capacity = nullptr;//transferring ownership.
}

template<class T>
List<T>::~List()
{
    if (_begin) {
        while (_end != _begin) {
            alloc.destroy(--_end);//"deleting"
        }
        alloc.deallocate(_begin, capacity());
    }
    _begin = _end = _capacity = nullptr;
}


template<class T>
List<T>& List<T>::operator=(const List<T> &rhs){
     if (this != &rhs) {
        if (_begin) {
            while (_end != _begin) {
                alloc.destroy(--_end);
            }
            alloc.deallocate(_begin, capacity());
        }

        _begin = alloc.allocate(rhs.size());//allocating stuff.
        _end = _begin;
        _capacity = _begin + rhs.size();
        for(size_t i = 0; i < rhs.size(); i++){
            alloc.construct(_end++, rhs._begin[i]);//constructing the list.
        }
    }

    return *this;

}

template <typename T>
List<T>& List<T>::operator=(List<T> &&rhs)
{
    if (this != &rhs) {
        if (_begin) {
            while (_end != _begin) {
                alloc.destroy(--_end);
            }
            alloc.deallocate(_begin, capacity());
        }
        _begin = rhs._begin;
        _end = rhs._end;
        _capacity = rhs._capacity;
        rhs._begin = rhs._end = rhs._capacity = nullptr;//tranferring the ownership.
    }

    return *this;
}

template <typename T>
void List<T>::grow() noexcept
{
    if (_begin == nullptr) {
        _begin = alloc.allocate(1);
        _end = _begin;
        _capacity = _end+1;
    }
    T * temp = alloc.allocate(capacity() * 2);//grows twice the capacity.
    T * tempEnd = temp;
    T * tempCap = temp + (capacity() * 2);

      for (T * i = _begin; i != _end; i++) {
        alloc.construct(tempEnd++, std::move(*i));
    }

    while (_end != _begin) {
        alloc.destroy(--_end);
    }
    alloc.deallocate(_begin, capacity());

    _begin = temp;
    _end = tempEnd;//assigning new values.
    _capacity = tempCap;

}

template <typename T>
void List<T>::displace(unsigned int i)
{
    if ( !(size() < capacity()) )
        throw std::range_error("displace has no room");

    for(size_t counter = size(); counter > i; counter--){
            _begin[counter]= _begin[counter -1];//shifting stuff.
        }
}

template <typename T>
void List<T>::collapse(unsigned int i) noexcept
{
    for (size_t counter = size(); counter < size() - 1; counter++)
        _begin[counter] = _begin[counter + 1];//erasing stuff.
}


template <typename T>
void List<T>::insert(const T& item) noexcept
{
    if (_capacity == nullptr || size() == capacity()) {
        grow();
    }

    if (size() == 0) {
        alloc.construct(&_begin[0],item);
        _end++;
        _capacity--;
        return;
    }

    size_t i = 0;
    while (i < size()) {
        if (*item < *(_begin[i])) {
            try {
                displace(i);
            } catch (std::range_error &e) {
                std::cerr << e.what() << " in insert function.\n";
            }
            break;
        }
        i++;
    }

    alloc.construct(&_begin[i], item);
    _end++;
}
template <typename T>
void List<T>::erase(T item)
{
    if (_end == nullptr)
        throw std::underflow_error("erase on empty");

    try {
        int loc = search(item);

        collapse(loc);
        _end--;
    } catch(std::domain_error &e) {
        throw std::range_error("erase non-existent item");
    }
}

template <typename T>
const T& List<T>::at(size_t i) const
{
    if (i < 0 || i >= size())
        throw std::out_of_range("bad index");

    return _begin[i];
}

template <typename T>
T& List<T>::at(size_t i)
{
    if (i < 0 || i >= size())
        throw std::out_of_range("bad index");

    return _begin[i];
}

template <typename T>
size_t List<T>::search(const T &obj) const
{
    for (size_t i = 0; i < size(); i++) {
        if (_begin[i] == obj) {
            return i;
        }
    }

    throw std::domain_error("does not exist");
}

#endif
