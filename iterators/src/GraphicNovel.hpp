/*
 * Filename:	GraphicNovel.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#ifndef GRAPHIC_NOVEL_HPP
#define GRAPHIC_NOVEL_HPP

#include "Book.hpp"

class GraphicNovel : public Book {
public:
    GraphicNovel(std::string last, std::string first, std::string illustrator, std::string title, int year);
private:
    std::string _illustrator;

    std::ostream& print(std::ostream &sout) const override;
};

#endif
