/*
 * Filename:	Book.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */

#ifndef BOOK_HPP
#define BOOK_HPP

#include <iostream>
#include <string>

class Book {
public:
    Book(std::string last, std::string first, std::string title, int year);
    std::string author() const;
    std::string title() const;
    int year() const;
    friend bool operator==(const Book &lhs, const Book &rhs);   // EXTRA
    friend bool operator!=(const Book &lhs, const Book &rhs);   // EXTRA
    friend bool operator< (const Book &lhs, const Book &rhs);
    friend bool operator<=(const Book &lhs, const Book &rhs);   // EXTRA
    friend bool operator> (const Book &lhs, const Book &rhs);   // EXTRA
    friend bool operator>=(const Book &lhs, const Book &rhs);   // EXTRA
    friend std::ostream& operator<<(std::ostream &sout, const Book &rhs);
protected:
    std::string _surname;
    std::string _forename;
    std::string _title;
    int _year;

    virtual std::ostream& print(std::ostream &sout) const = 0;
};

#endif
