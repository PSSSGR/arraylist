/*
 * Filename:	Novel.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#ifndef NOVEL_HPP
#define NOVEL_HPP

#include "Book.hpp"

class Novel : public Book {
public:
    Novel(std::string last, std::string first, std::string title, int year);
private:
    std::ostream& print(std::ostream &sout) const override;
};


#endif
