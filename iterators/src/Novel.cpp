/*
 * Filename:	Novel.cpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#include <iomanip>
#include "Novel.hpp"

Novel::Novel(
    std::string last,
    std::string first,
    std::string title,
    int year
) :
    Book(last, first, title, year)
{
}

std::ostream& Novel::print(std::ostream &sout) const
{
    char fill = std::cout.fill('.');

    sout << std::left << std::setw(10) << "Author: " << _forename << " " << _surname << std::endl
                      << std::setw(10) << "Title: " << _title << std::endl
                      << std::setw(10) << "Year: " << _year << std::endl;

    std::cout.fill(fill);

    return sout;
}
