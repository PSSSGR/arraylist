/*
 * Filename:	GraphicNovel.cpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#include"GraphicNovel.hpp"
#include<iostream>
#include<string>

GraphicNovel::GraphicNovel(std::string last,std::string first,std::string illus,std::string title,int year)
:Book(first,last,title,year),illus(illus){};//calling the base class constructor.
