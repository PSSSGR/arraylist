/*
 * Filename:	GraphicNovel.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */
#ifndef GRAPHIC_NOVEL_HPP
#define GRAPHIC_NOVEL_HPP

#include<iostream>
#include<string>
#include"Book.hpp"


class GraphicNovel:public Book {
    public:
        GraphicNovel(std::string last,std::string first,std::string illus,std::string title,int year);
        std::ofstream& print(std::ofstream& out)const override{
            out<<this->author();
            return(out);
        }
        std::string illustrator(){return(illus);};//getter.
    protected:
	    std::string illus;
};

#endif
