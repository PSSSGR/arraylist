/*
 * Template class that will be used to hold pointers to things
 * Sorted array list, depends on operator< existing for the type being held
*/
/*
 * Filename:	List.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */

#ifndef ARRAY_LIST_HPP
#define ARRAY_LIST_HPP

// Any necessary includes
#include<iostream>
#include<memory>
#include<stdexcept>
#include<list>


// It's a template class
template<class T>
class List {
public:
    // Default Constructor
    List();
    // Constructor that takes first item of list
    List(T first_elem);
    // Copy Constructor
    List(const List<T>& ListObject);
    // Move Constructor
    List(List<T>&& ListObject);
    // Destructor
    ~List();
    // Copy assignment operator overload
    List& operator = (const List<T>& ListObj);
    // Move assignment operator overload
    List& operator = (List<T>&& ListObj);
    // function size; does not throw exceptions
    size_t size()const noexcept {return(_size);};
    // function capacity; does not throw exceptions
    size_t capacity()const noexcept {return(_capacity);};
    // function insert; does not throw exceptions
    void insert(const T& element)noexcept;
    // function erase; throws underflow_error if empty, range_error if item doesn't exist
    void erase(T item);
    // function at; throws out_of_range exception; returns a const value
    const T& at(size_t index)const;
    // function at; throws out_of_range exception; returns l-value
    T& at(size_t index);
    // function search; throws domain error if item doesn't exist
    size_t search(T obj);
    bool operator ==(const T& object) const{return !(this != object);}
    //relational operators
    bool operator !=(const T& object)const {return (this != object);}
    size_t index(T item);
    void sort_list();
private:
    // variable that gives us our dynamic array
    T* dynamic_array;
    size_t _size;
    size_t _capacity;
    void increase_array();

};


// LIST CLASS FUNCTION IMPLEMENTATIONS
template<class T>
List<T> :: List()//Default Constructor.
    :dynamic_array(nullptr)
    ,_size(0)
    ,_capacity(0)

{
};

template<class T>
List<T> :: List(T first_elem)//constructo which takes first element.
:_size(1),_capacity(1)
{
    dynamic_array = new T[1];
    dynamic_array[0]= first_elem;
};

template<class T>
List<T>::List(const List<T>& ListObject)//copy constructor
    :_size(ListObject._size),_capacity(ListObject._size)
{
    dynamic_array = new T[_capacity];
	for (size_t i = 0; i<_size; i++){
	   dynamic_array[i] = ListObject.dynamic_array[i];//Copies the contents of the Object.
    }
   //   for (size_t i = 0; i < _size-1; i++)//sorts stuff
   //   {
   //     for (size_t j = i+1; j < _size; j++)
   //     {
   //        if (dynamic_array[i] > dynamic_array[j])
   //        {
   //           T temp = dynamic_array[i];
   //           dynamic_array[i] = dynamic_array[j];
   //           dynamic_array[j] = temp;
   //        }
   //     }
   // }
   //
   // sort_list();

}
template<class T>
List<T>::List(List<T>&& ListObject)//move constructor.
    :_size(ListObject._size),_capacity(ListObject._capacity)
{
    dynamic_array = new T[_capacity];
	dynamic_array = ListObject.dynamic_array;//tranfers the ownership.
	ListObject.dynamic_array = nullptr;//makes the object point to null.
    sort_list();
}

template<class T>
List<T>::~List(){
    delete [] dynamic_array;//gives memory back to the freestore.
    dynamic_array = nullptr;
}
template<typename T>
List<T>& List<T>::operator= (const List<T>& ListObj)//copy assignment.
{
    if(this != &ListObj){
        delete[] dynamic_array;
        dynamic_array = new T[ListObj._capacity];
        _capacity = ListObj._capacity;
        _size = ListObj._size;

        for(size_t i = 0; i<_size; i++)
            dynamic_array[i] = ListObj.dynamic_array[i];
    }
    return *this;
}
template<typename T>
List<T>& List<T>::operator= (List<T>&& ListObj)//move assignement
{
    if(this != &ListObj){
        delete[] dynamic_array;
        dynamic_array = new T[ListObj._capacity];
        _capacity = ListObj._capacity;
        _size = ListObj._size;
        dynamic_array = ListObj.dynamic_array;
        ListObj.dynamic_array = nullptr;
    }
    return *this;

}

template<class T>
void List<T>::increase_array(){
    if(_size == _capacity){
        T* temp_array = new T[_capacity*2];


        for (size_t i = 0; i < _size; ++i)
        {
            temp_array[i] = std::move(dynamic_array[i]);
        }
        delete [] dynamic_array;
        dynamic_array = temp_array;
        _capacity *=2;
    }

}

template<class T>
void List<T>::insert(T const& element)noexcept
{
    if(_size == 0){
        dynamic_array = new T[1];
         _size = 1;
         _capacity = 1;
         dynamic_array[0] = element;

    }else if (_size == _capacity){
            increase_array();
	        dynamic_array[_size] = element;
	        _size++;
    }

    sort_list();
}
template<class T>
size_t List<T>::search(T obj){
    size_t idx = index(obj);
    if(idx){
        return(idx);
    }else{
        throw(std::domain_error("domain_error"));//trows domain error exception.
    }
    return(idx);

}
template<class T>
size_t List<T>::index(T item){
    for(size_t i = 0;i<=_size;i++){
        if(item == dynamic_array[i]){
            return(i);
        }
    }
    return(-1);
}

template<class T>
void List<T>::erase(T item)
{
    if(_size == 0){
        throw  std::underflow_error("Empty List!");
    }
    size_t k = _size;

    size_t index  = this->search(item);
    if(index){
        for(size_t i =index ; i<=_size; i++){
            dynamic_array[i] = dynamic_array[i+1];
        }
        --_size;
    }
    if(_size != k){
        throw(std::range_error("range_error"));
    }
    --_size;

    sort_list();
}
template<class T>
const T& List<T>::at(size_t index)const{
    if(index < 0 || index >= _size){
        throw std::out_of_range("out_of_range");
    }else{
        return(dynamic_array[index]);
    }
}
template<class T>
T& List<T>::at(size_t index){
    if(index < 0 || index >= _size){
        throw std::out_of_range("out_of_range");
    }else{
        return(dynamic_array[index]);
    }
}
template<class T>
void List<T>::sort_list(){

   for (size_t i = 0; i < _size-1; i++)//sorts stuff with regarding to the names and titles.
   {

     for (size_t j = i+1; j < _size; j++)
     {
        if (dynamic_array[i] > dynamic_array[j])
        {

           T temp = dynamic_array[i];
           dynamic_array[i] = dynamic_array[j];
           dynamic_array[j] = temp;

        }
     }
 }


}

#endif
