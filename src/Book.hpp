/*
 * Filename:	Book.hpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */

#ifndef BOOK_HPP
#define BOOK_HPP

#include<iostream>
#include<string>
#include <fstream>


class Book {
//An abstract class.
    public:
        Book(std::string first,std::string last,std::string title,int year);
        virtual std::ofstream& print(std::ofstream& out)const = 0;//pure virtual
        friend std::ostream& operator << (const Book& Obj,std::ofstream& out){
            Obj.print(out);
            return(out);
        }
        bool operator !=(Book& Obj)const{//comparision ooperators.
            return !((this->author()==Obj.author()) && (this->title() ==Obj.title())&&(this->year() == Obj.year()));
        }
        bool operator < (Book& Obj)const;
        bool operator > (Book& Obj)const{return(Obj < this);}
        std::string author()const{return(First_name + " " + Last_name);}//getters and setters.
        std::string title()const{return(Title_name);}
        int year()const{return(published_year);}
        void set_first(std::string first){First_name=first;}
        void set_title(std::string title){Title_name = title;}
        void set_last(std::string last){Last_name = last;}
        void set_year(int time){published_year = time;}


    protected:
        std::string First_name;
        std::string Last_name;
        std::string Title_name;
	    int published_year;
};

#endif
