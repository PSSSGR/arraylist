/*
 * Filename:	Book.cpp
 * Name:		Sri Padala
 * Description:	This program is about implementing a templated arraylist class.
 */

#include"Book.hpp"


Book::Book(std::string first,std::string last,std::string title,int year)
:First_name(first),Last_name(last),Title_name(title),published_year(year){}

bool Book::operator <(Book& Obj)const{
    // std::string temp1 = First_name +" "+ Last_name;
    // std::string temp2 = Obj.author();
    // std::string temp3 = Title_name;
    // std::string temp4 = Obj.title();

    // if(temp1 > temp2 ){
    //     return(true);
    // }else if(temp1 == temp2){
    //     if(temp3 > temp4){
    //         return(true);
    //     }else{
    //         return(false);
    //     }
    // }else{
    //     return(false);
    // }
    if (Last_name < Obj.Last_name) {
        return true;
    } else if (Last_name == Obj.Last_name) {
        if (First_name < Obj.First_name) {
            return true;
        } else if (Title_name < Obj.Title_name) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}
